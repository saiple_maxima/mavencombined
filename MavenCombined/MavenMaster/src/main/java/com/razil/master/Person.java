package com.razil.master;

import com.razil.slave.*;
import lombok.Data;

@Data
public class Person {
    private String firstName;
    private String lastName;
    private Passport passport;

}
