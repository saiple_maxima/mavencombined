package com.razil.master.interfaces;

public interface Speaking extends Cloneable {

    void greeting();

    void bye();

}
