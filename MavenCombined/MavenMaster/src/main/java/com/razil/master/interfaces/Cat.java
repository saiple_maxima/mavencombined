package com.razil.master.interfaces;

public class Cat implements Speaking {

    @Override
    public void greeting() {
        System.out.println("Meow Hi");
    }

    @Override
    public void bye() {
        System.out.println("Meow Bye");
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
