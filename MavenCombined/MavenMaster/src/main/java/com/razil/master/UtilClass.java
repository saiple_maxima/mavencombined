package com.razil.master;

import lombok.experimental.UtilityClass;

@UtilityClass
public class UtilClass {

    public int sum(int a, int b){
        return a + b;
    }

}
