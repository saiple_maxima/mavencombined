package com.razil.master.interfaces;

public class Dog implements Speaking {
    @Override
    public void greeting() {
        System.out.println("Ruf Hi");
    }

    @Override
    public void bye() {
        System.out.println("Ruf Bye");
    }
}
